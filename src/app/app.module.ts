import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { AppComponent } from './app.component';

import { BannerComponent } from './components/banner/banner.component';
import { ButtonComponent } from './components/button/button.component';
import { NotificationComponent } from './components/notification/notification.component';
import { SelectComponent } from './components/select/select.component';
import { SubtitleComponent } from './components/subtitle/subtitle.component';
import { TextfieldComponent } from './components/textfield/textfield.component';
import { TitleComponent } from './components/title/title.component';
import { FormsModule } from '@angular/forms';

@NgModule({

  imports: [
    BrowserModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    BannerComponent,
    ButtonComponent,
    NotificationComponent,
    SelectComponent,
    SubtitleComponent,
    TextfieldComponent,
    TitleComponent
  ],
  entryComponents: [
    BannerComponent,
    ButtonComponent,
    NotificationComponent,
    SelectComponent,
    SubtitleComponent,
    TextfieldComponent,
    TitleComponent
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    const bannerElement = createCustomElement(BannerComponent, { injector });
    const buttonElement = createCustomElement(ButtonComponent, { injector });
    const notificationElement = createCustomElement(NotificationComponent, { injector });
    const selectElement = createCustomElement(SelectComponent, { injector });
    const subtitleElement = createCustomElement(SubtitleComponent, { injector });
    const textfieldElement = createCustomElement(TextfieldComponent, { injector });
    const titleElement = createCustomElement(TitleComponent, { injector });

    customElements.define('app-banner', bannerElement);
    customElements.define('app-button', buttonElement);
    customElements.define('app-notification', notificationElement);
    customElements.define('app-select', selectElement);
    customElements.define('app-subtitle', subtitleElement);
    customElements.define('app-textfield', textfieldElement);
    customElements.define('app-title', titleElement);

  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BannerComponent } from '../banner/banner.component';
import { ButtonComponent } from '../button/button.component';
import { NotificationComponent } from '../notification/notification.component';
import { SelectComponent } from '../select/select.component';
import { SubtitleComponent } from '../subtitle/subtitle.component';
import { TextfieldComponent } from '../textfield/textfield.component';
import { TitleComponent } from '../title/title.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BannerComponent,
    ButtonComponent,
    NotificationComponent,
    SelectComponent,
    SubtitleComponent,
    TextfieldComponent,
    TitleComponent
  ],
  exports: [
    BannerComponent,
    ButtonComponent,
    NotificationComponent,
    SelectComponent,
    SubtitleComponent,
    TextfieldComponent,
    TitleComponent
  ],
})
export class ViewLibModule { }

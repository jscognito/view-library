import { ViewLibModule } from './view-lib.module';

describe('ViewLibModule', () => {
  let viewLibModule: ViewLibModule;

  beforeEach(() => {
    viewLibModule = new ViewLibModule();
  });

  it('should create an instance', () => {
    expect(viewLibModule).toBeTruthy();
  });
});

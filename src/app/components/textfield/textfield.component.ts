import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-textfield',
  templateUrl: './textfield.component.html',
  styleUrls: ['./textfield.component.scss']
})
export class TextfieldComponent implements OnInit {

  valueInput: any;
  emptyInput: string;

  @Input() label: string;
  @Input() icon: string;
  @Input() type: string;
  @Input() state: string;
  @Input() message: string;

  constructor() {
  }

  ngOnInit() {
  }

  validateField() {
    (this.valueInput !== null && this.valueInput !== '') ? this.emptyInput = 'status' : this.emptyInput = '';
  }

}



import { TestBed, inject } from '@angular/core/testing';

import { ViewLibService } from './view-lib.service';

describe('ViewLibService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewLibService]
    });
  });

  it('should be created', inject([ViewLibService], (service: ViewLibService) => {
    expect(service).toBeTruthy();
  }));
});

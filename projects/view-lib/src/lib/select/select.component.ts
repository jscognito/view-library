import {Component, Input, OnInit} from '@angular/core';
@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  valueSelect = 0;
  emptySelect: string;

  @Input() label: string;
  @Input() icon: string;
  @Input() type: string;
  @Input() state: string;
  @Input() message: string;

  constructor() {
  }

  ngOnInit() {
  }

  validateField() {
    (this.valueSelect !== 0) ? this.emptySelect = 'status' : this.emptySelect = '';
  }

}

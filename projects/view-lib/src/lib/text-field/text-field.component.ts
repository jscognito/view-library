import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.scss']
})
export class TextFieldComponent implements OnInit {

  valueInput: any;
  emptyInput: string;

  @Input() label: string;
  @Input() icon: string;
  @Input() type: string;
  @Input() state: string;
  @Input() message: string;

  constructor() {
  }

  ngOnInit() {
  }

  validateField() {
    (this.valueInput !== null && this.valueInput !== '') ? this.emptyInput = 'status' : this.emptyInput = '';
  }

}



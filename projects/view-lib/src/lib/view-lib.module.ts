import { NgModule } from '@angular/core';
import { BannerComponent } from './banner/banner.component';
import { ButtonComponent } from './button/button.component';
import { NotificationComponent } from './notification/notification.component';
import { SelectComponent } from './select/select.component';
import { SubtitleComponent } from './subtitle/subtitle.component';
import { TextFieldComponent } from './text-field/text-field.component';
import { TitleComponent } from './title/title.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    FormsModule
  ],
  declarations: [
    BannerComponent,
    ButtonComponent,
    NotificationComponent,
    SelectComponent,
    SubtitleComponent,
    TextFieldComponent,
    TitleComponent
  ],
  exports: [
    BannerComponent,
    ButtonComponent,
    NotificationComponent,
    SelectComponent,
    SubtitleComponent,
    TextFieldComponent,
    TitleComponent
  ]
})
export class ViewLibModule { }
